#!/usr/bin/env python

import Bio.PDB
import proteinomicon
import os
import json
import argparse

import warnings
warnings.filterwarnings('ignore')

def get_indices(fh):
	obj = json.load(fh)
	newobj = {}

	for pdbc in obj:
		if pdbc[:4] not in newobj: newobj[pdbc[:4]] = {}
		newobj[pdbc[:4]][pdbc[-1]] = obj[pdbc]
	return newobj

def get_composition(model, indices):

	counts = {}
	for chain in model:
		if chain.id not in indices: continue

		for residue in chain:
			if not proteinomicon.is_aa(residue): continue
			if proteinomicon.in_spans(residue.id[1], indices[chain.id]):
				resn = Bio.PDB.protein_letters_3to1[residue.get_resname()]
				if resn in counts: counts[resn] += 1
				else: counts[resn] = 1

	return counts

def main(deutdir, **kwargs):

	all_models = kwargs.get('all_models', False)

	pdblist = set()
	with open('{}/deuterocol1/pdblist.json'.format(deutdir)) as f:
		obj = json.load(f)
		for fam in obj: [pdblist.add(pdbid[:4]) for pdbid in obj[fam]]
	parser = Bio.PDB.PDBParser()

	indices = get_indices(open('{}/deuterocol1/indices.json'.format(deutdir)))
	tmcounts = {}
	for pdb in indices:
		tmcounts[pdb[:4]] = 0
		for chain in indices[pdb]:
			tmcounts[pdb[:4]] += len(indices[pdb][chain])

	istruc = 0
	for pdb in sorted(pdblist):
		pdbfn = '{}/deuterocol1/pdbs/{}.pdb'.format(deutdir, pdb)
		try: struc = parser.get_structure(str(istruc), pdbfn)
		except TypeError as e: 
			print('#ERROR: {}'.format(e))
			continue

		for model in struc:
			out = '#Structure: {}#{}\n'.format(pdb, model.id)

			composition = get_composition(model, indices[pdb[:4]])
			out += '#Total chains: {}\n'.format(len(indices[pdb[:4]]))
			out += '#Total TMSs: {}\n'.format(tmcounts[pdb[:4]])
			out += '#Total TMS residues: {}\n'.format(sum(composition.values()))
			out += '#Composition: {}\n'.format('\t'.join([str(composition.get(resn, 0)) for resn in 'ACDEFGHIKLMNPQRSTVWY']))

			clusters = proteinomicon.cb_aabb_scan(model)
			clusters = proteinomicon.get_clusters_in_spans(clusters, indices[pdb[:4]])
			clusters = proteinomicon.cb_get_clusters_within(clusters)
			out += '#Total contacts: {}\n'.format(proteinomicon.count_edges(clusters)//2)
			out += proteinomicon.cb_tabulate_contacts(clusters)

			print(out)

			if not all_models: break
		istruc += 1

			
if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('indir')
	parser.add_argument('--all-models', action='store_true', help='Process all models and not just the first one')

	args = parser.parse_args()

	main(args.indir, all_models=args.all_models)
