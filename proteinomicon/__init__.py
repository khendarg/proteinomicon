
#References
#Adhikari B., Cheng J. (2016) Protein Residue Contacts and Prediction Methods. In: Carugo O., Eisenhaber F. (eds) Data Mining Techniques for the Life Sciences. Methods in Molecular Biology, vol 1415. Humana Press, New York, NY

import Bio.PDB
import numpy as np
import json

def is_aa(residue):
	if isinstance(residue, str): resn = residue
	else: resn = residue.get_resname()

	if resn in Bio.PDB.protein_letters_3to1: return True
	else: return False

def cb_aabb_scan(model, cutoff=10, minseq=6):
	'''
	Clusters together residues with beta carbons (or alpha carbons, in the case of glycine) within a certain bounding box. This greatly reduces the number of useless checks on obviously distant residues in later steps.

	model: A single Biopython Model object
	cutoff: Bounding box half-width. 7-8\AA is typical, but 10\AA is used here on account of this being a coarse method. (default:10)
	minseq: Minimum sequence distance permitted (default:6)

	TODO: Implement ligand scanning properly for those delicious protein-ligand interactions
	'''
	clusters = {}
	aalist = []
	for chain in model:
		for residue in chain:
			if is_aa(residue):
				if 'CA' in residue: aalist.append(residue)

	n = 0
	for res1 in aalist:

		if 'CB' in res1: pt1 = res1['CB'].get_coord()
		else: pt1 = res1['CA'].get_coord()

		for res2 in aalist:
			if res1 == res2: continue
			if abs(res1.get_id()[1] - res2.get_id()[1]) < minseq: continue
			if res2 < res1: continue

			if 'CB' in res2: pt2 = res2['CB'].get_coord()
			else: pt2 = res2['CA'].get_coord()

			taxicab = np.max(np.abs(pt2 - pt1))
			#print(res1.id[1], res2.id[1], taxicab, cutoff)
			if taxicab <= cutoff:
				if res1 not in clusters: clusters[res1] = set()
				if res2 not in clusters: clusters[res2] = set()
				clusters[res1].add(res2)
				clusters[res2].add(res1)
				n += 2

	return clusters

def ca_aabb_scan(model, cutoff=12, minseq=6):
	'''
	Clusters together residues with alpha carbons within a certain bounding box. This greatly reduces the number of useless checks on obviously distant residues in later steps.

	model: A single Biopython Model object
	cutoff: Bounding box half-size. 7-8\AA is typical, but 12\AA is used here on account of this being a coarse method. (default:10)
	minseq: Minimum sequence distance permitted (default:6)

	TODO: Implement ligand scanning properly for those delicious protein-ligand interactions
	'''
	clusters = {}
	aalist = []
	for chain in model:
		for residue in chain:
			if is_aa(residue):
				if 'CA' in residue: aalist.append(residue)

	n = 0
	for res1 in aalist:
		for res2 in aalist:
			if res1 == res2: continue
			if abs(res1.get_id()[1] - res2.get_id()[1]) < minseq: continue

			if res2 < res1: break

			taxicab = np.max(np.abs(res2['CA'].get_coord() - res1['CA'].get_coord()))
			if taxicab <= cutoff:
				if res1 not in clusters: clusters[res1] = set()
				if res2 not in clusters: clusters[res2] = set()
				clusters[res1].add(res2)
				clusters[res2].add(res1)
				n += 2
	return clusters

def get_indices(indicesfh, prefix):
	obj = json.load(indicesfh)
	indices = {}
	for k in obj:
		if k.startswith(prefix): indices[k[-1]] = obj[k]
	return indices

def in_spans(resi, spans):
	for span in spans:
		if resi in range(span[0], span[-1]+1): return True
	return False

def count_edges(table): return sum([len(table[k]) for k in table])

def get_clusters_in_spans(clusters, spans):
	newclusters = {}
	n = 0
	for res1 in clusters:

		chain1 = res1.get_parent().id
		if chain1 not in spans: continue

		if in_spans(res1.get_id()[1], spans[chain1]): 

			for res2 in clusters[res1]:

				chain2 = res2.get_parent().id
				if chain2 not in spans: continue

				if in_spans(res2.get_id()[1], spans[chain2]):
					if res1 not in newclusters: newclusters[res1] = set()
					newclusters[res1].add(res2)
					n += 1
	return newclusters

def cb_get_clusters_within(clusters, cutoff=8):
	newclusters = {}
	for res1 in clusters:
		if 'CB' in res1: pt1 = res1['CB'].get_coord()
		else: pt1 = res1['CA'].get_coord()

		for res2 in clusters[res1]:
			if 'CB' in res2: pt2 = res2['CB'].get_coord()
			else: pt2 = res2['CA'].get_coord()

			dist = np.linalg.norm(pt2 - pt1)
			if dist <= cutoff:
				if res1 not in newclusters: newclusters[res1] = set()
				if res2 not in newclusters: newclusters[res2] = set()

				newclusters[res1].add(res2)
				newclusters[res2].add(res1)

	return newclusters

def pymolize(clusters):
	colors = ['red', 'yellow', 'green', 'cyan', 'blue', 'magenta']
	cmd = ''
	done = set()
	for i, k in enumerate(clusters): 
		idstr = '+'.join([str(r.id[1]) for r in clusters[k] if ((k,r) not in done and (r,k) not in done)])
		for r in clusters[k]: 
			done.add((k,r))
			done.add((r,k))
		line = 'color {}, i. {}+{}\n'.format(colors[i%6], k.id[1], idstr)
		if line.endswith('+\n'): continue
		else: cmd += line
	return cmd

def pymolize_all(clusters):
	indices = set()
	for k in clusters:
		indices.add(k.id[1])
		for r in clusters[k]:
			indices.add(r.id[1])
	return 'color red, i. {}'.format('+'.join([str(i) for i in indices]))

def cb_tabulate_contacts(clusters):
	done = set()
	out = ''
	for res1 in sorted(clusters):
		if 'CB' in res1: pt1 = res1['CB'].get_coord()
		else: pt1 = res1['CA'].get_coord()

		for res2 in sorted(clusters[res1]):
			if (res2,res1) in done: continue

			if 'CB' in res2: pt2 = res2['CB'].get_coord()
			else: pt2 = res2['CA'].get_coord()

			resns = sorted([Bio.PDB.protein_letters_3to1[r] for r in [res1.get_resname(), res2.get_resname()]])

			out += '{}/{}{}\t{}/{}{}\t{:0.4f}\n'.format(res1.parent.id, resns[0], res1.id[1], res2.parent.id, resns[1], res2.id[1], np.linalg.norm(pt2 - pt1))
			done.add((res1,res2))
	return out
