#!/usr/bin/env python

import proteinomicon
import unittest

class TestReadSingle(unittest.TestCase):
	pdbid = '4P79'
	pdbfn = 'proteinomicon_sample/deuterocol1/pdbs/4P79.pdb'
	indicesfn = 'proteinomicon_sample/deuterocol1/indices.json'
	parser = proteinomicon.Bio.PDB.PDBParser()

	def test_00_load_pdb(self):
		struc = self.parser.get_structure(self.pdbfn, self.pdbfn)
		for model in struc:
			assert len(model) == 1
			for chain in model:
				assert len(chain) > 200
				for residue in chain:
					assert len(residue) >= 1
					if proteinomicon.is_aa(residue): assert len(residue) >= 4

	def test_10_broad_cluster(self):
		struc = self.parser.get_structure(self.pdbfn, self.pdbfn)
		for model in struc:
			resclusters = proteinomicon.cb_aabb_scan(model)

	def test_20_tm_contacts(self):
		with open(self.indicesfn) as f: indices = proteinomicon.get_indices(f, self.pdbid)
			
		struc = self.parser.get_structure(self.pdbfn, self.pdbfn)
		for model in struc:
			resclusters = proteinomicon.cb_aabb_scan(model)
			resclusters = proteinomicon.get_clusters_in_spans(resclusters, indices)

	def test_30_euclidean_contacts(self):
		with open(self.indicesfn) as f: indices = proteinomicon.get_indices(f, self.pdbid)
			
		struc = self.parser.get_structure(self.pdbfn, self.pdbfn)
		for model in struc:
			resclusters = proteinomicon.cb_aabb_scan(model)
			#print(proteinomicon.count_edges(resclusters))
			resclusters = proteinomicon.get_clusters_in_spans(resclusters, indices)
			#print(proteinomicon.count_edges(resclusters))
			resclusters = proteinomicon.cb_get_clusters_within(resclusters, 8)
			#print(proteinomicon.count_edges(resclusters))
			#print(proteinomicon.pymolize_all(resclusters))

	def test_40_tabulate_contacts(self):
		with open(self.indicesfn) as f: indices = proteinomicon.get_indices(f, self.pdbid)
			
		struc = self.parser.get_structure(self.pdbfn, self.pdbfn)
		for model in struc:
			resclusters = proteinomicon.cb_aabb_scan(model)
			resclusters = proteinomicon.get_clusters_in_spans(resclusters, indices)
			resclusters = proteinomicon.cb_get_clusters_within(resclusters, 8)

			print(proteinomicon.cb_tabulate_contacts(resclusters))

if __name__ == '__main__':
	unittest.main()
